#!/bin/bash

docker exec -it tatoo-kc /opt/keycloak/bin/kc.sh export --dir /tmp/export
docker cp tatoo-kc:/tmp/export ./data/keycloak
