# dieAte's Keycloak Sandbox

This project is Ate's test setup to demonstrate and experiment with several keycloak features.
It contains a docker-compose to start a keycloak instance on localhost, that can be setup by terraform scripts.
The authentication can be tested by a little demo app, where a user can login and logout and read several user data like roles.

## Getting started using Docker
You can use docker for setting up a development environment.

Start docker container:

    docker-compose -f docker-compose.dev.yml up

## Access to Keycloak

https://localhost:8383    
http://localhost:8282
  
User: kcadmin
Password: kcadmin

### Export/Import State
Current realm state could be exported to './data/keycloak/export' using this script:

    ./export-keycloak-data.sh

To import realm data activate import param in docker-compose.local.yml OR execute this script:

    ./import-keycloak-data.sh

## Terraform

### Terraform Based Setup
To set up the keycloak by terraform you need to install terraform on your computer.

https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
https://computingforgeeks.com/how-to-install-terraform-on-fedora/

Keycloak has to be started.
Then enter the /terraform subdirectory:

    cd ./terraform

From terraform dir call:

    terraform init
    terraform validate
    terraform plan -out=local.plan
    terraform apply -input=false local.plan
