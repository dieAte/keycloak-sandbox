import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StartpageComponent} from "./startpage/startpage.component";
import {TestAuthGuardGuard} from "./auth/test-auth-guard.guard";

const routes: Routes = [
  { path: 'home', component: StartpageComponent, canActivate: [TestAuthGuardGuard] },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
