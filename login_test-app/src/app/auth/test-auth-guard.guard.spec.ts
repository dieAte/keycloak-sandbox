import { TestBed } from '@angular/core/testing';

import { TestAuthGuardGuard } from './test-auth-guard.guard';

describe('TestAuthGuardGuard', () => {
  let guard: TestAuthGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TestAuthGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
