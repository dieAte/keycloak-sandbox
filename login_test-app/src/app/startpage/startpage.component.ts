import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { from, lastValueFrom, map } from 'rxjs';

class User {
  public firstName?: string;
  public lastName?: string;
  public email?: string;
  public userName?: string;

  constructor(u: Partial<User>) {
    return Object.assign(this, u);
  }

  getUserName() {
    return `${this.firstName ?? ''} ${this.lastName ?? ''}`.trim() || this.userName;
  }
}

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent implements OnInit {
  public user: User | undefined;

  constructor(
    private keycloak: KeycloakService
  ) { }

  ngOnInit(): void {
    this.getUserProfile();
  }

  async doLogout(): Promise<void> {
    await this.keycloak.logout();
  }

  private async getUserProfile(): Promise<void> {
    const user$ =  from(this.keycloak.loadUserProfile()).pipe(
      map(u => new User({
        firstName: u.firstName,
        lastName: u.lastName,
        email: u.email,
        userName: u.username
      })
    ));
    this.user = await lastValueFrom(user$);
  }

}
