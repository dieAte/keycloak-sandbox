const dieateLocalSettings: any = {
  keycloakConfig: {
    url: 'https://localhost:8383',
    realm: 'dieate_test',
    clientId: 'dieate_app_access_public'
  }
};

const usedSettings = dieateLocalSettings;
export const environment = {
  production: false,
  keycloak: usedSettings.keycloakConfig,
};
