variable "kc_url" {
  description = "keycloak base url"
  type        = string
}

variable "kc_initial_admin_username" {
  description = "username of keycloak admin for initial startup - same as initial KEYCLOAK_ADMIN from docker-compose env variables"
  type        = string
}
variable "kc_initial_admin_password" {
  description = "password of keycloak admin for initial startup - same as initial KEYCLOAK_ADMIN_PASSWORD from docker-compose env variables"
  type        = string
  sensitive   = true
}

variable "kc_master_admin_username" {
  description = "username of keycloak admin set up by terraform - if you want to use this admin for terraform cli switch username in provider keycloak to var.kc_master_admin_username AND KEYCLOAK_ADMIN in docker-compose env variables postgres-keycloak.env"
  type        = string
}
variable "kc_master_admin_password" {
  description = "password of keycloak admin set up by terraform - if you want to use this admin for terraform cli switch password in provider keycloak to var.kc_master_admin_password AND KEYCLOAK_ADMIN_PASSWORD in docker-compose env variables postgres-keycloak.env"
  type        = string
  sensitive   = true
}
variable "kc_master_admin_email" {
  description = "keycloak admin email address"
  type        = string
}

variable "kc_email_smtp_host" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - SMTP host name"
  type        = string
}
variable "kc_email_smtp_username" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - username for authentication to the SMTP server"
  type        = string
  sensitive   = true
}
variable "kc_email_smtp_pwd" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - password for authentication to the SMTP server"
  type        = string
  sensitive   = true
}
variable "kc_email_from_address" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - email address used as from sender"
  type        = string
}
variable "kc_email_from_display_name" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - display name used as from sender"
  type        = string
}
variable "kc_email_reply" {
  description = "keycloak email setup for email correspondence (registration, forgot password, ...) - email address for reply-to"
  type        = string
  default     = "support@devnull.de"
}

