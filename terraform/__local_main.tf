provider "keycloak" {
  client_id                = "admin-cli"
  username                 = var.kc_initial_admin_username
  password                 = var.kc_initial_admin_password  #  read from env via TF_VAR_kc-admin-password
  url                      = var.kc_url
  tls_insecure_skip_verify = true
}
