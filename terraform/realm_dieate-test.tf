####################################
# dieate_test realm
####################################

resource "keycloak_realm" "realm_dieate_test" {
  for_each                       = local.dieate_app_realms
  realm                          = each.key
  display_name                   = each.value.description
  display_name_html              = "<div class=\"kc-logo-text\"><span>${each.value.description}</span></div>"
  enabled                        = true
  login_theme                    = "dieate-app"
  user_managed_access            = false
  registration_allowed           = true
  registration_email_as_username = false
  login_with_email_allowed       = true
  duplicate_emails_allowed       = false
  reset_password_allowed         = true
  verify_email                   = true
  ssl_required                   = "all"
  internationalization {
    supported_locales = [
      "de",
      "en",
    ]
    default_locale = "de"
  }
  password_policy = "upperCase(1) and lowerCase(1) and digits(1) and length(8) and notUsername and notEmail and passwordHistory(3)"
  smtp_server {
    host              = var.kc_email_smtp_host
    from              = var.kc_email_from_address
    from_display_name = var.kc_email_from_display_name
    reply_to          = var.kc_email_reply
    starttls          = true

    auth {
      username = var.kc_email_smtp_username
      password = var.kc_email_smtp_pwd
    }
  }
  security_defenses {
    headers {
      x_frame_options                     = "SAMEORIGIN"
      content_security_policy             = "frame-src 'self'; frame-ancestors 'self'; object-src 'none';"
      content_security_policy_report_only = ""
      x_content_type_options              = "nosniff"
      x_robots_tag                        = "none"
      x_xss_protection                    = "1; mode=block"
      strict_transport_security           = "max-age=31536000; includeSubDomains"
    }
    brute_force_detection {
      permanent_lockout                = false
      max_login_failures               = 5
      wait_increment_seconds           = 30
      quick_login_check_milli_seconds  = 1000
      minimum_quick_login_wait_seconds = 60
      max_failure_wait_seconds         = 600
      failure_reset_time_seconds       = 43200
    }
  }
}

data "keycloak_realm_keys" "dieate_test" {
  realm_id   = try(keycloak_realm.realm_dieate_test["dieate_test"].id, "unknown")
  algorithms = ["RS256"]
  status     = ["ACTIVE"]
}

####################################
# dieate app
####################################

locals {
  all_dieate_app_clients = flatten([
    for realmKey, realm in local.dieate_app_realms : [
      for clientKey, client in local.dieate_app_clients : {
        key : "${realmKey}_${clientKey}"
        value : merge({
          name : clientKey
          realm : try(keycloak_realm.realm_dieate_test[realmKey], "unknown")
        }, client)
      }
    ]
  ])

  all_dieate_app_clients_map = { for item in local.all_dieate_app_clients : item.key => item.value }

  all_dieate_app_client_roles = flatten([
    for clientKey, client in local.all_dieate_app_clients_map : [
      for role, rv in local.dieate_app_client_roles : {
        key : "${clientKey}_${role}"
        value : merge({
          name : role
          realm : client.realm
          client : try(keycloak_openid_client.dieate_app_clients[clientKey], "unknown")
      }, rv) }
    ]
  ])

  all_dieate_app_client_roles_map = { for item in local.all_dieate_app_client_roles : item.key => item.value }
}


data "keycloak_openid_client" "dieate_test_account_client" {
  for_each  = local.dieate_app_realms
  realm_id  = try(keycloak_realm.realm_dieate_test[each.key].id, "unknown")
  client_id = "account"
}

data "keycloak_role" "dieate_test_account_view_profile_role" {
  for_each  = local.all_dieate_app_clients_map
  realm_id  = try(each.value.realm.id, "unknown")
  client_id = try(data.keycloak_openid_client.dieate_test_account_client[each.value.realm.id].id, "unknown")
  name      = "view-profile"
}

# dieate app clients ####################
resource "keycloak_openid_client" "dieate_app_clients" {
  for_each                                 = local.all_dieate_app_clients_map
  realm_id                                 = try(each.value.realm.id, "unknown")
  client_id                                = each.value.name
  access_type                              = each.value.confidential ? "CONFIDENTIAL" : "PUBLIC"
  base_url                                 = var.dieate_app_base_url
  description                              = each.value.description
  direct_access_grants_enabled             = true
  exclude_session_state_from_auth_response = false
  name                                     = each.value.description
  standard_flow_enabled                    = true
  valid_redirect_uris = [
    local.dieate_app_login_redirect_url
  ]
  valid_post_logout_redirect_uris = [
    local.dieate_app_logout_redirect_url
  ]
  web_origins        = ["+"]
  full_scope_allowed = false
}

## dieate app roles ####################

resource "keycloak_role" "dieate_app_roles" {
  for_each    = local.all_dieate_app_client_roles_map
  realm_id    = each.value.realm.id
  client_id   = each.value.client.id
  name        = each.value.name
  description = each.value.description
}

resource "keycloak_openid_hardcoded_role_protocol_mapper" "dieate_app_hardcoded_role_mapper" {
  for_each  = local.all_dieate_app_clients_map
  realm_id  = try(each.value.realm.id, "unknown")
  client_id = try(keycloak_openid_client.dieate_app_clients[each.key].id, "unknown")
  name      = "hardcoded_role_mapper_dieate_app_user"
  role_id   = try(keycloak_role.dieate_app_roles["${each.key}_dieate_app_user"].id, "unknown")
}

resource "keycloak_generic_role_mapper" "dieate_app_client_account_profile_mapper" {
  for_each  = local.all_dieate_app_clients_map
  realm_id  = try(each.value.realm.id, "unknown")
  client_id = try(keycloak_openid_client.dieate_app_clients[each.key].id, "unknown")
  role_id   = try(data.keycloak_role.dieate_test_account_view_profile_role[each.key].id, "unknown")
}

resource "keycloak_generic_role_mapper" "dieate_app_client_roles_mapper" {
  for_each  = local.all_dieate_app_client_roles_map
  realm_id  = each.value.realm.id
  client_id = each.value.client.id
  role_id   = try(keycloak_role.dieate_app_roles[each.key].id, "unknown")
}

############################################################
## google IDP setup for external users
############################################################

resource "keycloak_oidc_google_identity_provider" "google" {
  for_each      = local.dieate_app_realms
  realm         = try(keycloak_realm.realm_dieate_test[each.key].id, "unknown")
  client_id     = var.dieate_test_google_identity_provider_client_id
  client_secret = var.dieate_test_google_identity_provider_client_secret
  trust_email   = true
  sync_mode     = "FORCE"
  enabled       = true
}
