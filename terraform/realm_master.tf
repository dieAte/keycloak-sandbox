data "keycloak_realm" "realm_master" {
  realm = "master"
}

data "keycloak_role" "master_admin_role" {
  realm_id  = data.keycloak_realm.realm_master.id
  name      = "admin"
}
resource "keycloak_user" "master_admin" {
  realm_id       = data.keycloak_realm.realm_master.id
  username       = var.kc_master_admin_username
  email          = var.kc_master_admin_email
  email_verified = true
  initial_password {
    value     = var.kc_master_admin_password
    temporary = false
  }
}

resource "keycloak_user_roles" "master_admin_assign_admin_role" {
  realm_id = data.keycloak_realm.realm_master.id
  user_id  = keycloak_user.master_admin.id

  role_ids = [
    data.keycloak_role.master_admin_role.id
  ]
}
