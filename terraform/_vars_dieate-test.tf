variable "dieate_test_google_identity_provider_client_id" {
  description = "google IDP client-ID"
  type        = string
  sensitive   = true
}

variable "dieate_test_google_identity_provider_client_secret" {
  description = "google IDP client-key"
  type        = string
  sensitive   = true
}

locals {
  dieate_test_realm = "${var.kc_url}/realms/dieate_test"
}
