variable "dieate_app_base_url" {
  description = "dieate App Base URL"
  type        = string
}

locals {
  dieate_app_login_redirect_url  = "${var.dieate_app_base_url}/*"
  dieate_app_logout_redirect_url = "${var.dieate_app_base_url}/*"

  dieate_app_realms = {
    "dieate_test" = {
      description = "dieate Realm for External Users"
    }
  }

  dieate_app_clients = {
    "dieate_app_access_confidential" = {
      confidential = true,
      description  = "User Access to dieate App (confidential->with secret)"
    },
    "dieate_app_access_public" = {
      confidential = false,
      description  = "User Access to dieate App (public->without secret)"
    }
  }

  dieate_app_client_roles = {
    "dieate_app_user" = {
      description = "dieate App User (in general)",
    },
    "dieate_app_test_user" = {
      description = "Test User who tests new features of the dieate App",
    }
  }

  dieate_app_client_roles_IDP_mapping = {
    "dieate_app_test_user" = {
      description     = "Test User who tests new features of the dieate App",
      googleGroupName = "dieate-app-test-user"
    }
  }

  dieate_app_internal_users_connector = "idp_connector_4_dieate_employee"

  dieate_app_internal_users_broker_alias = "idp_4_dieate_employee"

  idp_connector_4_dieate_employee_valid_redirect_urls = [for k, v in local.dieate_app_realms : "${var.kc_url}/auth/realms/${k}/broker/${local.dieate_app_internal_users_broker_alias}/endpoint"]
  idp_connector_4_dieate_employee_valid_logout_redirect_url = [for url in local.idp_connector_4_dieate_employee_valid_redirect_urls : "${url}/endpoint"]
}
