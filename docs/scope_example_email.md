# OAuth 2.0 Scopes

## Definition

Scope values are used to specify what access privileges are being requested for Access Tokens.

Scopes are added in the consent. After the resource owner accepts to give the client the right
to access these scopes, the scope values (attributes) that are assigned to a scope
are added to the access token.

Protected Resource endpoints MAY perform different actions and return different information
based on the scope values and other parameters from the presented Access Token used to ask for access
to that  OAuth 2.0 protected endpoint.

Example:
Scope: email
This scope value requests access to the End-User's email Claims, which are:
* email
* email_verified

## Example: Scopes in Keycloak

### Define Client Scopes

In the Client Scope Area you can define the client scopes, if they are available for SAML or OIDC,
and if per default this scope should preselected as default or optional in the clients.
![Keycloak-Scopes: General Client Scopes Setup](keycloak_screenshots/kc_scopes_01_general_client_scopes.png)

Here you also define the mappers for attributes that shall be added to the tokens for this scope.
![Keycloak-Scopes: Email Scope Parameters](keycloak_screenshots/kc_scopes_02_email_scope_params.png)

Here is an example for an OIDC scope. It defines the type of mapping, the 
Token Claim Name (key of this property in the token) 
and that this property shall be added to the access token but also to the ID token and userinfo (see OIDC documentation).
![Keycloak-Scopes: Email Parameter Settings](keycloak_screenshots/kc_scopes_02b_email_attribute_settings.png)

### Independent Client Scopes Settings for Every Client
For every client you can define scopes independently, based on the scope settings in the Client Scope Area.
![Keycloak-Scopes: Client Scope Definition in Client](keycloak_screenshots/kc_scopes_03_client_client_scopes.png)

If you activate the "email" scope..
![Keycloak-Scopes: Set email as default](keycloak_screenshots/kc_scopes_04_email_default.png)

..the two claims set for the "email" scope are added to the access token.
![Keycloak-Scopes: Access token with email scope](keycloak_screenshots/kc_scopes_05_access_token_with_email.png)

If you set the "email" scope to optional..
![Keycloak-Scopes: Set email as optional](keycloak_screenshots/kc_scopes_06_email_optional.png)

..the "email" scope appears in the additional Scope Parameters in the evaluation tab.
(maybe you need to reload the page after toggling the email scope settings on the scopes list)
![Keycloak-Scopes: email can be added dynamically](keycloak_screenshots/kc_scopes_06b_email_optional_dropdown.png)

When "email" scope is deactivated the email claims "email" and "email_verified"
are no longer part of the access token.
![Keycloak-Scopes: Access token without email scope](keycloak_screenshots/kc_scopes_07_access_token_without_email.png)
