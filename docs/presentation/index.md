---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundImage: linear-gradient( to right bottom, rgba(126, 213, 111, 0.4), rgba(40, 180, 133, 0.6)), url('../img/keycloak-bg.png')
---

![bg left:40% 80%](../img/keycloak_icon_512px.svg)

# **Marp**

Markdown Presentation Ecosystem

https://marp.app/

---

# How to write slides

Split pages by horizontal ruler (`---`). It's very simple! :satisfied:

```markdown
# Slide 1

foobar

---

# Slide 2

foobar
```
