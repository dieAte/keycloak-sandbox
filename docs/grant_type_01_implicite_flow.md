# Grant Type: Implicit Grant Flow (with POST)

Easiest flow:

1. The user clicks Login in the app
2. the app routes the user to the Auth0 Authorization Server (/authorize endpoint) 
3. If not authorised the Authorization Server redirects the user to the login form
4. The user authenticates (by username/pwd or another configured login options)
and may see a consent page with list of requested scopes for the app
5. Authorization Server redirects the user back to the app with an ID Token

![001_oauth2_implicite_flow_with_login-OAUTH2___Implicit_Flow.png](OAuth2_OIDC_Flows%2F001_oauth2_implicite_flow_with_login-OAUTH2___Implicit_Flow.png)

* Easiest Flow, used long time ago when the OAuth2 protocol was introduced
* Reason for the implicit flow: cross domain request were not allowed
  * Last step in default OAuth2 authorisation code flow is to perform a POST request with the code
  to the AuthServer to get an access token
  * That is a cross domain request if your AuthServer runs under another URL than you App
  * (now we can specify which locations are allowed for cross domain request → CORS,
  valid redirect URIs)
* only the clientId is used

Problem:

**More ways to steel the redirect than to intercept the code-to-token exchange POST request**

If the access-token is transported via the URL in a GET request it's most unsecure:
* traffic logging
* browser history → especially if you synchronise your browser history with Google!!!
* browser plugins if you gave them access to the browser's address bar

Or like captive WiFi Portals work:

* instead of loading the redirected page the portals login page is shown
* if the bearer token is part of this redirect, the portal server get the bearer token → game over!

## Links and Sources

https://auth0.com/docs/get-started/authentication-and-authorization-flow/implicit-flow-with-form-post