# OIDC - Open Id Connect





## ID Tokens vs Access Tokens

Access tokens are what the OAuth client uses to make requests to an API.
An ID token contains information about what happened when a user authenticated.


* Access tokens are meant to be read by the resource server
* ID tokens are meant to be read by the OAuth client
* Access tokens should never be read by the client
* ID tokens should never be sent to an API
* The ID token _**MAY**_ also contain information about the user such as their name or email address (not a requirement of an ID token) 
* Access tokens are defined in OAuth
* ID tokens are defined in OpenID Connect

## Links and Sources

https://oauth.net/id-tokens-vs-access-tokens/