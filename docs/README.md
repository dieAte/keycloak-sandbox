# Ate's Keycloak Sandbox Documentation

## Authorisation and Authentication Concepts

Here you can find documentation about:

1. [OAuth2](oauth2.md)
2. [OIDC](oidc.md)