# Ates's Keycloak Sandbox

This project contains:
* a docker-compose to startup a keycloak with postgres db, ssl and other example setups
* a basic login themes
* a test angular app where users can login, logout and show check their roles
* terraform scripts to setup keycloak realms by scripts

## Documentation

Documentation about this project, authorisation- and authentication concepts and keycloak can be found
[here](docs/README.md).

## Getting started using Docker

You can use docker for setting up a development environment.

Start docker container:

``docker-compose -f docker-compose.local.yml up``

## keycloak

After starting docker container Keycloak is started on port 8282 with http and on port 8383 with ssl.
You can access keycloak:

* http://localhost:8282 or https://localhost:8383
* User: kcadmin
* Password: kcadmin

### Export/Import State

**ATTENTION!!!! Import only works if realm doesn't already exist. Better use terraform scripts for an ongoing configuration.**


Current realm state could be exported to './data/keycloak/export' using this script:

```commandline
./export-keycloak-data.sh
```
After that you can set the auto import param on keycloak startup in the docker-compose file with activation this param.
Or manually execute this script:

```commandline
./import-keycloak-data.sh
```

## Requirements

* Terraform
   

## Terraform Based Setup

To set up the keycloak by terraform you need to install terraform on your computer.

  * https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
  * https://computingforgeeks.com/how-to-install-terraform-on-fedora/

Keycloak have to be running.
Then enter the /terraform subdirectory:

```
cd ./terraform
```

From terraform dir call:

```
terraform init
terraform validate
terraform plan -var-file values.tfvars -out=local.plan
terraform apply -input=false local.plan
```
