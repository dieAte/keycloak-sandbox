#!/bin/bash

docker cp ./data/keycloak/export kc-tatoo:/tmp/export
docker exec -it kc-tatoo /opt/keycloak/bin/kc.sh import --dir /tmp/export
